# Python example using PyTorch

## Pre-requisite
* Install pipenv globally
```shell
$ pip install pipenv
```

## Checkout & Build

```
* `git clone` git@gitlab.com:enterpriseexamples/others/pytorch-lab.git
* `cd` pytorch-lab
```

* _Optional_: Install environment with contents from Pipfile. Generates Pipfile.lock
```shell
pipenv install
```

* Create virtual environment by dowloading libraries from Pipfile.lock
```shell
make venv_dev
```

### Misc
* Find out where your virtual environment is:
```shell
$ pipenv --venv
```
This should show the `.venv` is within your project directory

* Find out where your project home is:
```shell
$ pipenv --where
```

## Run the application
* Start the `pipenv` shell
```shell
$ pipenv shell
```

* Run the application
```shell
$ pipenv run python -m 01_tensors
```

## Reference
* [PyTorch](https://pytorch.org/tutorials/beginner/blitz/tensor_tutorial.html#sphx-glr-beginner-blitz-tensor-tutorial-py)

* https://blog.exxactcorp.com/kickstart-your-deep-learning-with-these-3-pytorch-projects/
* https://www.oracle.com/solutions/chatbots/what-is-a-chatbot/
