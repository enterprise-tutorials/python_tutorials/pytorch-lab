from __future__ import print_function
import torch

# Construct a 5x3 matrix
x1 = torch.empty(5, 3)
print("X1: Empty torch: ", x1)

# Construct a randomly initialized matrix:
x2 = torch.rand(5, 3)
print("X2: Rand torch: ", x2)

# Construct a matrix filled zeros and of dtype long:
x3 = torch.zeros(5, 3, dtype=torch.long)
print("X3: Long torch: ", x3)

# Construct a tensor directly from data:
x4 = torch.tensor([5.5, 3])
print("X4: From Data: ", x4)

# Create tensor based on existing tensor
x5 = x1.new_ones(5, 3, dtype=torch.double)      # new_* methods take in sizes
print("X5 : ", x5)

x6 = torch.randn_like(x2, dtype=torch.float)    # override dtype!
print("X6 : ", x6)                                      # result has the same size

# Print the size
print("Size of X6: ", x6.size())

# Addition
print("Add X5 & X6: ", torch.add(x5, x6))

# Addition: Providing output sensor as an argument
result = torch.empty(5, 3)
torch.add(x5, x6, out=result)
print("Add X5 & X6 with argument: ", result)
